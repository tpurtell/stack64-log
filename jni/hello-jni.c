#include <string.h>
#include <jni.h>
#include <pthread.h>
#include <android/log.h>

/* 
	CODE ADAPTED FROM: https://github.com/mono/mono/blob/5e0579782e13c8093ff9b58307ea8a3f3fa09726/mono/utils/mono-threads-posix.c#L235
*/
jstring
Java_com_example_hellojni_HelloJni_stringFromJNI( JNIEnv* env,
                                                  jobject thiz )
{

	unsigned char* _staddr;
	size_t _stsize;
	
	unsigned char** staddr = &_staddr;
	size_t* stsize = &_stsize;

	/* Linux, BSD */

	pthread_attr_t attr;
	unsigned char *current = (unsigned char*)&attr;

	*staddr = NULL;
	*stsize = (size_t)-1;

	pthread_attr_init (&attr);

	pthread_getattr_np (pthread_self(), &attr);

	pthread_attr_getstack (&attr, (void**)staddr, stsize);
	pthread_attr_destroy (&attr);

	__android_log_print(ANDROID_LOG_DEBUG, "STACK64", "stack address current %08x, base %08x, size %08x\n", current, _staddr, _stsize);

	if (!((current > *staddr) && (current < *staddr + *stsize))) {
		__android_log_print(ANDROID_LOG_DEBUG, "STACK64", "failing stack address range check\n");
	} else {
		__android_log_print(ANDROID_LOG_DEBUG, "STACK64", "passed stack address range check\n");
	}
	__android_log_print(ANDROID_LOG_DEBUG, "STACK64", "assert was %08x > %08x && %08x < %08x\n", current, *staddr, current, *staddr + *stsize);

    return (*env)->NewStringUTF(env, "Hello from JNI !");
}
